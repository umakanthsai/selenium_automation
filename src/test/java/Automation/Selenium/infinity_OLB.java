package Automation.Selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;



public class infinity_OLB {

	static WebDriver driver;
	
	public void commonSteps() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\c.umakanthsai\\Documents\\Projects\\Selenium_maven\\Selenium_Automation\\web_drivers\\chrome_99\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		driver = new ChromeDriver(options);
		driver.get("https://infinitysmet24.konycloud.com/apps/Onlinebanking/#/AuthenticationMA/frmLogin");
		driver.manage().window().maximize();
		Thread.sleep(5000);

	}

	public void login(WebDriver driver, String userName) throws InterruptedException {

		driver.findElement(By.xpath("//div[@class='sknBorderE3E3E3']/input")).sendKeys(userName);
		driver.findElement(By.xpath("//div[@class='sknBorderE3E3E3'][2]/input")).sendKeys("Kony@1234");
		driver.findElement(By.xpath("//div[@class='slFbox']/button[@class='-kony-ca-middlecenter sknBtnNormalSSPFFFFFF15Px']")).click();
		Thread.sleep(15000);
	}

	public void navigate_ApprovalMatrix(WebDriver driver) throws InterruptedException {
		
		driver.findElement(By.xpath("//div[@class='slFboxRound']/div[@title='Settings']")).click();
		driver.findElement(By.xpath("//div[@class='sknFlxffffff']/label[@title='Approval Matrix']")).click();
		Thread.sleep(5000);
	
		String ApprovalMatrix_manageApprovals = driver.findElement(By.xpath("//div[@kwp='frmApprovalmatrix_profileMenu_APPROVALMATRIXflxSubMenu']/div[1]/label")).getText();
		Assert.assertEquals("Manage Approvals", ApprovalMatrix_manageApprovals);
		
		driver.findElement(By.xpath("//div[@class='slFbox']/button")).click();
		Thread.sleep(10000);
	}
	
	public static void main(String[] args) throws InterruptedException {

		infinity_OLB obj = new infinity_OLB();
		obj.commonSteps();
		obj.login(driver, "7766306185");
		obj.navigate_ApprovalMatrix(driver);
		driver.quit();
	
		
	}

}
