package Automation.Selenium;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

public class sec7_dropdowns {

	static WebDriver driver;
	static String url = "https://www.spicejet.com";

	/*
	 * Facing Issue with isSelected() method in 'spice jet' login page
	 */
	
	public void common_steps() {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\c.umakanthsai\\Documents\\Projects\\Selenium_maven\\Selenium_Automation\\web_drivers\\chrome_99\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		driver = new ChromeDriver(options);
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));

	}

	public void validateBookingType(WebDriver driver) {
		// Validating the Booking Type:

		String[] bookingType = driver.findElement(By.xpath(
				"//div[contains(@class,'css-1dbjc4n r-1d09ksm r-qklmqi r-1lz4bg0 r-1phboty r-18u37iz')]/div[1]/div[2]"))
				.getAttribute("class").split("r-ubezar");
		String[] booking_Type = bookingType[0].split(" ");
		String type = booking_Type[1].trim();
		System.out.println(type);

		for (int i = 1; i <= 4; i++) {
			String bookingtypes = driver.findElement(By
					.xpath("//div[contains(@class,'css-1dbjc4n r-1d09ksm r-qklmqi r-1lz4bg0 r-1phboty r-18u37iz')]/div["
							+ i + "]/div[2]"))
					.getAttribute("class");
			if (bookingtypes.contains(type)) {
				String booking_type = driver.findElement(By.xpath(
						"//div[contains(@class,'css-1dbjc4n r-1d09ksm r-qklmqi r-1lz4bg0 r-1phboty r-18u37iz')]/div["
								+ i + "]/div[2]"))
						.getText();
				System.out.println("Selected Booking Type is " + booking_type);
				Assert.assertEquals("Flights", booking_type);
				break;
			}
		}

	}
	
	public void onewayTripDetails(WebDriver driver) throws InterruptedException {
		
		//Make sure it is a one way trip
		
		driver.findElement(By.xpath("//div[text()='one way']")).click();
		driver.findElement(By.xpath("//div[@class='css-1dbjc4n r-13awgt0 r-18u37iz']/div[1]/div[1]/div[2]/input")).sendKeys("BLR");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//div[@class='css-1dbjc4n r-13awgt0 r-18u37iz']/div[3]/div[1]/div[2]/input")).sendKeys("MAA");
		
		//Selecting the Date
		
		driver.findElement(By.xpath("//div[@class='css-76zvg2 r-jwli3a r-ubezar r-16dba41']")).click();
		
		String styleValue = driver.findElement(By.xpath("//div[@class='css-1dbjc4n r-1awozwy r-18u37iz r-1wtj0ep']/child::div[text()='Select Date']")).getAttribute("style");
		String opacityValue = styleValue.substring(31, 34);
		if(styleValue.contains(opacityValue)) 
			Assert.assertTrue(true, "Select Date is disabled");
		else
			Assert.assertTrue(false, "Select Date is enabled");	
		
		//Passengers
		driver.findElement(By.xpath("//div[text()='Passengers']/parent::div/div[2]")).click();
		driver.findElement(By.xpath("//div[text()='Adult']/ancestor::div[@class='css-1dbjc4n r-18u37iz r-1wtj0ep r-1x0uki6']/div[2]/div[3]")).click();
		driver.findElement(By.xpath("//div[text()='Children']/ancestor::div[@class='css-1dbjc4n r-18u37iz r-1wtj0ep r-1x0uki6']/div[2]/div[3]")).click();
		System.out.println(driver.findElement(By.xpath("//div[text()='Passengers']/parent::div/div[2]")).getText());
		
		//Currency
		WebElement currenyType = driver.findElement(By.xpath("//div[text()='Currency']/parent::div/div[2]/div"));
		System.out.println(currenyType.getText());
		currenyType.click();
		Thread.sleep(3000);
		WebElement usd = driver.findElement(By.xpath("//div[text()='USD']/parent::div"));
		usd.click();
		System.out.println(currenyType.getText().equals("USD"));
		
		//template type
		driver.findElement(By.xpath("//div[text()='Students']/parent::div[@class='css-1dbjc4n']")).click();
	}
	
	public void search(WebDriver driver2) throws InterruptedException {
		driver.findElement(By.xpath("//div[@class='css-76zvg2 r-jwli3a r-1b6yd1w r-1kfrs79']/parent::div[@data-testid='home-page-flight-cta']")).click();
		Thread.sleep(6000);
	}

	public static void main(String[] args) throws InterruptedException {
		
		sec7_dropdowns obj = new sec7_dropdowns();
		
		obj.common_steps();
		obj.validateBookingType(driver);
		obj.onewayTripDetails(driver);
		obj.search(driver);

		Thread.sleep(8000);
		driver.quit();

	}

}
