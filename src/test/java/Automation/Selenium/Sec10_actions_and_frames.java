package Automation.Selenium;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Sec10_actions_and_frames {

	private static String url = "https://the-internet.herokuapp.com/";
	private static WebDriver driver;

	public void driverObject() {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\c.umakanthsai\\Documents\\Projects\\Selenium_maven\\Selenium_Automation\\web_drivers\\chrome_99\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		driver = new ChromeDriver(options);
		driver.get(url);
		driver.manage().window().maximize();
	}

	public void sec10Assignment() {
		driver.findElement(By.linkText("Multiple Windows")).click();
		driver.findElement(By.linkText("Click Here")).click();
		Set<String> windows = driver.getWindowHandles();
		Iterator<String> windowIterator = windows.iterator();
		String parentWindow = windowIterator.next();
		String childWindow = windowIterator.next();
		driver.switchTo().window(childWindow);
		System.out.println(driver.findElement(By.xpath("//h3[text()='New Window']")).getText());
		driver.switchTo().window(parentWindow);
		System.out.println(driver.findElement(By.xpath("//h3[text()='Opening a new window']")).getText());
	}

	public void sec10AssignmentOnFrames() {
		driver.navigate().back();
		driver.findElement(By.linkText("Nested Frames")).click();
		System.out.println(driver.findElements(By.tagName("frame")).size());
		WebElement frameTop = driver.findElement(By.xpath("//frame[@name='frame-top']"));
		driver.switchTo().frame(frameTop);
		WebElement frameMiddle = driver.findElement(By.xpath("//frame[@name='frame-middle']"));
		driver.switchTo().frame(frameMiddle);

		System.out.println(driver.findElement(By.id("content")).getText());
	}

	public static void main(String[] args) {

		Sec10_actions_and_frames obj = new Sec10_actions_and_frames();
		obj.driverObject();
		obj.sec10Assignment();
		obj.sec10AssignmentOnFrames();

		driver.quit();

	}

}
