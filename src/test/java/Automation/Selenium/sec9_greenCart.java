package Automation.Selenium;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class sec9_greenCart {

	public static WebDriver driver;
	public static String url = "https://rahulshettyacademy.com/seleniumPractise/#/";

	public void commonStrpsforURL(String url) {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\c.umakanthsai\\Documents\\Projects\\Selenium_maven\\Selenium_Automation\\web_drivers\\chrome_99\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		driver = new ChromeDriver(options);
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	}

	public void addingItems(WebDriver driver) throws InterruptedException {
		String[] items = { "Tomato", "Beans", "Corn", "Cucumber" };
		System.out.println(items.length);
		JavascriptExecutor js = (JavascriptExecutor) driver;

		for (int i = 0; i < items.length; i++) {
			if (items[i].equals("Cucumber")) {
				js.executeScript("window.scrollBy(0,-1000)");
			}

			driver.findElement(
					By.xpath("//h4[text()='" + items[i] + " - 1 Kg']/ancestor::div[@class='product']/div[3]/button"))
					.click();

			Thread.sleep(3000);
		}
	}

	public static void main(String[] args) throws InterruptedException {

		sec9_greenCart obj = new sec9_greenCart();
		obj.commonStrpsforURL(url);
		obj.addingItems(driver);

		driver.quit();

	}

}
